const router = require('express').Router()
// Controller
const Product = require('../controller/productController')

// middleware
const uploader = require('../middlewares/uploader')
const Authentication = require('../middlewares/authenticate')

// API server
router.post('/create', Authentication, uploader.single('image'), Product.createProduct)
router.get('/', Product.findProducts)
router.get('/:id', Product.findProductById)
router.put('/update/:id', Product.updateProduct)
router.delete('/delete/:id', Product.deleteProduct)

module.exports = router