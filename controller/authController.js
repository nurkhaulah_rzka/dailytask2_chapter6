const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');
const { User, Warehouse } = require('../models')

const register = async (req, res) => {
    const { name, email, password, warehouseId } = req.body
    try {
        // untuk enkripsi password
        const hashedPassword = bcrypt.hashSync(password, 10)

        // untuk cari atau buat baru warehouse
        const [warehouse, newWarehouse] = await Warehouse.findOrCreate({
            where: {
                id: warehouseId,
            },
            defaults: { name: `Toko ${name}` },
        })

        // untuk validasi, jika warehouse id nya sudah terpakai, maka gagal melanjutkan
        if (!newWarehouse) {
            res.status(400).json({
                status: 'fail',
                message: 'Sorry, warehouse ID already taken!'
            })
        }

        // untuk register user baru
        const newUser = await User.create({
            name,
            email,
            password: hashedPassword,
            warehouseId
        })

        res.status(201).json({
            status: 'success',
            data: {
                newUser,
                warehouse,
                newWarehouse
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const login = async (req, res) => {
    const { email, password} = req.body
    try {
        // untuk cari user berdasarkan username
        const user = await User.findOne({
            where: {
                email
            }
        })

        // untuk pemberitahuan bahwa gagal melanjutkan karena username nya tidak ada 
        if (!user) {
            res.status(400).json({
                status: "failed",
                message: "Sorry, user doesn't exist!"
            })
        }

        // generate token untuk user yang success login
        const token = jwt.sign({
            id: user.id,
            username: user.username,
            warehouseId: user.warehouseId
        }, 'rahasia')

        // untuk check password user, jika success login maka dapat response intinya TOKEN
        if (user && bcrypt.compareSync(password, user.password)) {
            res.status(200).json({
                status: 'Success',
                data: {
                    username: user.username,
                    warehouseId: user.warehouseId,
                    token
                }
            })
        } else {
            res.status(400).json({
                status: "failed",
                message: "Sorry, wrong password!"
            })
        }
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const user = async (req, res) => {
    res.status(200).json({
        status: 'Success',
        data: {
            userId: req.user.id,
            username: req.user.username,
            warehouseId: req.user.warehouseId,
        }
    })
}

module.exports = {
    register,
    login,
    user,
}
